# ISLEWARD

A multiplayer, moddable, extensible roguelike built with NodeJS, JS, HTML and CSS

### Installation
1. Download and install NodeJS: `https://nodejs.org/en/download/`
2. Open a new console window
2. Get the code: `git clone https://gitlab.com/Isleward/isleward.git`
3. Navigate to the server folder: `src/server`
4. Install dependencies: `npm install`
5. Run: `node --expose-gc index.js`